//
//  ViewController.h
//  p01-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/5/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UILabel *helloWorldLabel;
    IBOutlet UIButton *showButton;
    IBOutlet UIButton *hideButton;
}

-(IBAction)showButton:(id)sender;
-(IBAction)hideButton:(id)sender;

@end

