//
//  AppDelegate.h
//  p01-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/5/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

