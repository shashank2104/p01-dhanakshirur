//
//  ViewController.m
//  p01-dhanakshirur
//
//  Created by Shashank Dhanakshirur on 2/5/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
-(IBAction)showButton:(id)sender
{
    helloWorldLabel.hidden = NO;
    showButton.hidden = YES;
    hideButton.hidden = NO;
}

-(IBAction)hideButton:(id)sender
{
    helloWorldLabel.hidden = YES;
    showButton.hidden = NO;
    hideButton.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    helloWorldLabel.hidden = YES;
    helloWorldLabel.textColor = [UIColor redColor];
    showButton.hidden = NO;
    hideButton.hidden = YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
